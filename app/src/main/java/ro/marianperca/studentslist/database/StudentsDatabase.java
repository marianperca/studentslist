package ro.marianperca.studentslist.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import ro.marianperca.studentslist.database.dao.StudentDao;
import ro.marianperca.studentslist.database.model.Student;

/**
 * Created by marian on 02/05/2018.
 */

@Database(entities = {Student.class}, version = 1)
public abstract class StudentsDatabase extends RoomDatabase {
    static final String DATABASE_NAME = "students_db";

    private static StudentsDatabase database;

    public static StudentsDatabase getInstance(Context context) {
        if (null == database) {
            database = Room.databaseBuilder(
                    context,
                    StudentsDatabase.class,
                    DATABASE_NAME
            ).build();
        }

        return database;
    }

    public abstract StudentDao studentDao();
}
