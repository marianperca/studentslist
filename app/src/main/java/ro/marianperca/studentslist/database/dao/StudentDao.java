package ro.marianperca.studentslist.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ro.marianperca.studentslist.database.model.Student;

/**
 * Created by marian on 02/05/2018.
 */

@Dao
public interface StudentDao {

    @Query("SELECT * FROM students")
    List<Student> getAll();

    @Insert
    void insert(Student s);

}
