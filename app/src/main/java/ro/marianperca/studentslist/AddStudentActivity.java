package ro.marianperca.studentslist;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import java.lang.ref.WeakReference;

import ro.marianperca.studentslist.database.StudentsDatabase;
import ro.marianperca.studentslist.database.model.Student;

public class AddStudentActivity extends AppCompatActivity implements View.OnClickListener {

    EditText mName;
    EditText mEmail;
    EditText mAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        mName = findViewById(R.id.name);
        mEmail = findViewById(R.id.email);
        mAge = findViewById(R.id.age);

        findViewById(R.id.btn_save).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Student s = new Student();
        s.setName(mName.getText().toString());
        s.setEmail(mEmail.getText().toString());
        s.setAge(Integer.parseInt(mAge.getText().toString()));

        (new InsertTask(this)).execute(s);
    }

    private static class InsertTask extends AsyncTask<Student, Void, Boolean> {

        private WeakReference<AddStudentActivity> activityRef;

        public InsertTask(AddStudentActivity act) {
            this.activityRef = new WeakReference<AddStudentActivity>(act);
        }

        @Override
        protected Boolean doInBackground(Student... students) {
            for (Student s : students) {
                StudentsDatabase.getInstance(activityRef.get())
                        .studentDao()
                        .insert(s);
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (activityRef.get() != null) {
                activityRef.get().setResult(1);
                activityRef.get().finish();
            }
        }
    }
}
