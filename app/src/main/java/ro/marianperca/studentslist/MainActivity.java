package ro.marianperca.studentslist;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import ro.marianperca.studentslist.database.StudentsDatabase;
import ro.marianperca.studentslist.database.model.Student;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private StudentAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.students_list);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
        mRecyclerView.addItemDecoration(itemDecor);

        mAdapter = new StudentAdapter(new ArrayList<Student>());
        mRecyclerView.setAdapter(mAdapter);

        findViewById(R.id.add_student).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AddStudentActivity.class);
                startActivityForResult(i, 1);
            }
        });

        (new GetStudentsTask(this)).execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        (new GetStudentsTask(this)).execute();
    }

    // clasa care reprezinta un rand
    class StudentViewHolder extends RecyclerView.ViewHolder {
        View mRow;
        TextView mName;
        TextView mEmail;

        StudentViewHolder(View v) {
            super(v);

            mRow = v;
            mName = v.findViewById(R.id.name);
            mEmail = v.findViewById(R.id.email);
        }
    }

    public class StudentAdapter extends RecyclerView.Adapter<StudentViewHolder> {

        private List<Student> students;

        public StudentAdapter(ArrayList<Student> mDataset) {
            students = mDataset;
        }

        public void addStudents(List<Student> elements) {
            students = elements;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public StudentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_item, parent, false);

            return new StudentViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull StudentViewHolder holder, int position) {
            final Student s = students.get(position);

            holder.mName.setText(s.getName());
            holder.mEmail.setText(s.getEmail());
            holder.mRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(MainActivity.this, s.getName(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public int getItemCount() {
            return students.size();
        }
    }

    private static class GetStudentsTask extends AsyncTask<Void, Void, List<Student>> {

        private WeakReference<MainActivity> activityRef;

        public GetStudentsTask(MainActivity act) {
            this.activityRef = new WeakReference<MainActivity>(act);
        }

        @Override
        protected List<Student> doInBackground(Void... voids) {
            return StudentsDatabase.getInstance(activityRef.get()).studentDao().getAll();
        }

        @Override
        protected void onPostExecute(List<Student> students) {
            if (activityRef.get() != null) {
                activityRef.get().mAdapter.addStudents(students);
            }
        }
    }
}
